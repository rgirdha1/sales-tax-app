var express = require("express");
var bodyParser = require("body-parser");
var app = express();
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').load();
}
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-azure-ad').BearerStrategy;
var ConnectionPool = require('tedious-connection-pool');
const tds = require('./app/utils/tds-promises');
var Tedious = require('tedious');
var Request = Tedious.Request;
var Connection = Tedious.Connection;
const transactionController = require("./app/controllers/transactionController")
const receiptController = require("./app/controllers/receiptController")
const aggregationController = require("./app/controllers/aggregationController")
const userController = require('./app/controllers/userController')

var port = process.env.port || 3000

var poolConfig = {
    min: 2,
    max: 4,
    log: true
};

var config = {
    userName: process.env.SqlUsername,
    password: process.env.SqlPassword,
    server: process.env.SqlServer,
    port: 1433,
    options: {
        database: process.env.SqlDatabase,
        rowCollectionOnRequestCompletion: true,
        requestTimeout: 30000
    }
};

tds.default.setConnectionPool(new ConnectionPool({}, config));

//var basicAuthCreds = JSON.parse(process.env.Basic_Auth_Conn_String);

app.use(bodyParser.json({limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true }));


//Authentication module
app.use(passport.initialize());

/*
passport.use(new BasicStrategy(async (username, password, done) => {
    try {
        for(var cred in basicAuthCreds){
            if(username === basicAuthCreds[cred].username && password === basicAuthCreds[cred].key){
                return done(null, true);
            }
        }  
        return done(null, false, {message: 'Invalid client_id or api_key'});
    }
    catch (ex) {
        done(ex);
    }
})); */

// OAuth bearer strategy for  apis
var aad_auth_options = {  
    identityMetadata: process.env.AADMetadataEndpoint,
    clientID: process.env.ClientId,
   // audience: process.env.AADAudience,
    isB2C: true,
    validateIssuer: false,
    loggingLevel: 'info',
    passReqToCallback: false,
    policyName: process.env.PolicyName,
   // ignoreExpiration: true, 
    loggingNoPII: false,
    scope: ['taxAPI.Read', 'taxAPI.Write']
};

var findById = function(id, fn) {
    for (var i = 0, len = users.length; i < len; i++) {
        var user = users[i];
        Console.log("user: " + user)
        if (user.oid === id) {
            log.info('Found user: ', user);
            return fn(null, user);
        }
    }
    return fn(null, null);
}

/*
var bearerStrategy = new BearerStrategy(aad_auth_options, 
    function(token, done) {
        findById(token.oid, function(err, user){
            if(err) {
                return done(err)
            }
            if (!user) {                            //Not registring new users for now. ****Check this later: https://docs.microsoft.com/en-us/azure/active-directory-b2c/active-directory-b2c-devquickstarts-api-node#add-authentication-to-your-rest-api-server
                return done('User not found')
            }
            owner = token.oid
            return done(null, user, token)
        });
    });
*/

var bearerStrategy = new BearerStrategy(aad_auth_options,
    function(token, done) {
        done(null, {}, token)
    });

// Strategy for any valid AAD user
passport.use(bearerStrategy);

// Strategy for valid AAD users in our admin group
/*passport.use("aad-admin", new BearerStrategy(aad_auth_options, async (token, done) => { 
    // Verify that the user associated with the supplied token is a member of our specified group
    if (await adminUserCache.isUserAdmin(token.oid)) {
        token['is_admin'] = true;
        return done(null, token);
    }
    done(null, false);
}));*/
// Note: All routes with 'basic' auth also accept OAuth bearer as well
/*var basicAuthStrategy = () => passport.authenticate(['basic', 'aad-user'], {session: false});*/

//var bearerOAuthStrategy = (requireAdmin) => passport.authenticate(requireAdmin ? 'aad-admin' : 'aad-user', {session: false});
var bearerOAuthStrategy = () => passport.authenticate('oauth-bearer', {session: false});

var router = express.Router();

router.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

var stdHandler = (handler) => {
    return (req, res) => {
        handler(req, resp => {
            if (resp.code == 200) {
                return res.json(resp.msg);
            }
            else {
                return res.status(resp.code).send(resp.msg);
            }
        });
    }
};

router.get('/', function(req, res) {
    res.json({ message: 'Welcome to Sales Tax App api!' });   
});


router.route('/tax/:trxId?')
    .get(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => transactionController.getLastTrx(req.authInfo, req.params.trxId, resultDispatcher)))
    .post(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => transactionController.postNewTrx(req.authInfo, req.body, resultDispatcher)))
    .delete(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => transactionController.deleteTrx(req.authInfo, req.params.trxId, req.body, resultDispatcher)));

router.route('/receipt/:trxId')
    .get(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => receiptController.getReceipt(req.authInfo, req.params.trxId, req.query.trx_date, req.query.receipt_name, resultDispatcher)))
    .post(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => receiptController.postReceipt(req.authInfo, req.params.trxId, req.body, resultDispatcher)))
    .delete(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => receiptController.deleteReceipt(req.authInfo, req.params.trxId, req.body, resultDispatcher)));

router.route('/agg/list/:pageNo?')
    .get(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => aggregationController.getTaxList(req.authInfo, req.params.pageNo, resultDispatcher)));

router.route('/agg/ytdtotal/:year?')
    .get(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => aggregationController.getYTDTotal(req.authInfo, req.params.year, resultDispatcher)));

router.route('/deleteAll')
    .delete(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => userController.deleteAll(req.authInfo, resultDispatcher)));

router.route('/newuserinit')
    .post(bearerOAuthStrategy(), stdHandler((req, resultDispatcher) => userController.newUserInit(req.authInfo, resultDispatcher)));
    //Start Here -> Test All functions again, especially deleteReceipt and deleteTrx

app.use('/api', router);

var server = app.listen(port, function () {
    console.log("Listening on port %s...", port);
});

module.exports = {app};