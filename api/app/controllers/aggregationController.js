var TYPES = require('tedious').TYPES;
var ConnectionPool = require('tedious-connection-pool');
var Tedious = require('tedious');
var Request = Tedious.Request;
var Connection = Tedious.Connection;
var env = require('dotenv').load();
const tds = require('../utils/tds-promises');
var formidable = require('formidable');
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').load();
}

async function getTaxList (claims, pageNo, output) {
    try {
        var scp = claims['scp']
        var username = claims['name']
        if(scp.split(" ").indexOf(process.env.ReadScope) >= 0) {
            var numPages = -1
            const perPage = 25       //Fixed page size of 25
            if(pageNo == null) {
                pageNo = 1
                var sqlStmt = "SELECT count(id) as totalCount from dbo.transactions a where a.username = @username";
                let results = await tds.default.sql(sqlStmt)
                    .parameter('username', TYPES.NVarChar, username)
                    .executeImmediate();
                if (!results || results.length != 1) {
                    numPages = 0
                    return output({code: 404, msg: "Unable to find any transactions for username: " + username});
                } else {
                    var dataSize = results[0].totalCount
                    numPages = Math.ceil(dataSize/perPage)
                }
            }

            var Sql = "SELECT id, username, entry_datetime, trx_date, category, memo, sales_tax, grand_total, receipt_link " +  
                      "FROM dbo.transactions where username = @username " + 
                      "ORDER BY id DESC " + 
                      "OFFSET @PageSize * (@PageNumber - 1) ROWS " + 
                      "FETCH NEXT @PageSize ROWS ONLY;"

            let results = await tds.default.sql(Sql)
                .parameter('username', TYPES.NVarChar, username)
                .parameter('PageSize', TYPES.Int, parseInt(perPage))
                .parameter('PageNumber', TYPES.Int, parseInt(pageNo))
                .executeImmediate();

            if (!results || results.length == 0) {
                return output({code: 404, msg: "Unable to find any transactions for username: " + username});
            } else {
                return output({code: 200, msg: {numPages: numPages, results: results.map(row => {
                    return {
                        Id: row.id,
                        TransactionDate: row.trx_date,
                        Category: row.category,
                        Memo: row.memo,
                        SalesTax: row.sales_tax,
                        GrandTotal: row.grand_total,
                        Receipt: row.receipt_link
                        };
                    })}
                });
            }      
        }  else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    } catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.getTaxList = getTaxList;

async function getYTDTotal (claims, year, output) {
    try {
        var scp = claims['scp']
        var username = claims['name']
        if(scp.split(" ").indexOf(process.env.ReadScope) >= 0) {
            let results = await getYTDTotal_Internal(username, year);
            if(year != null) {
                if(results.length == 0) {
                    output({code: 404, msg: 'No taxes found for year: ' + year})
                } else {
                    output({code: 200, msg: results[0]})
                }
            } else {
                output({code: 200, msg: results})
            }
        }  else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    }  catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.getYTDTotal = getYTDTotal;

async function getYTDTotal_Internal(username, year) {
    return new Promise(async (resolve, reject) => {
        try {
            var sqlStmt = "SELECT * from dbo.ytdtotal a where a.username = @username "
            if(year != null) {
                sqlStmt += "and a.year = @year "
            }
            sqlStmt += "ORDER BY a.year DESC";

            var sql = tds.default.sql(sqlStmt)
                            .parameter('username', TYPES.NVarChar, username);
            
            if(year != null) {
                sql.parameter('year', TYPES.Int, year)
            }

            let results = await sql.executeImmediate();
            resolve(results.map(row => {
                return {
                    Year: row.year,
                    SalesTax: row.sales_tax,
                    GrandTotal: row.grand_total
                };
            }));
        } catch (ex) {
            reject(ex)
        }
    })
}
module.exports.getYTDTotal_Internal = getYTDTotal_Internal