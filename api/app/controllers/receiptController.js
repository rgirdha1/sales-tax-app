var TYPES = require('tedious').TYPES;
var ConnectionPool = require('tedious-connection-pool');
var Tedious = require('tedious');
var Request = Tedious.Request;
var Connection = Tedious.Connection;
const aggController = require('./aggregationController')
const functions = require('../utils/functions')
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').load();
}
const tds = require('../utils/tds-promises');
const {
    SharedKeyCredential,
    uploadStreamToBlockBlob,
    downloadBlobToBuffer,
    Aborter,
    BlobURL,
    BlockBlobURL,
    ContainerURL,
    ServiceURL,
    StorageURL
  } = require("@azure/storage-blob");


const STORAGE_ACCOUNT_NAME = process.env.storageAccName;
const ACCOUNT_ACCESS_KEY = process.env.storageAccKey;
const ONE_MEGABYTE = 1024 * 1024;
const FOUR_MEGABYTES = 4 * ONE_MEGABYTE;
const ONE_MINUTE = 60 * 1000;
const credentials = new SharedKeyCredential(STORAGE_ACCOUNT_NAME, ACCOUNT_ACCESS_KEY);
const pipeline = StorageURL.newPipeline(credentials, {
    retryOptions: {maxTries: 4}
});
const serviceURL = new ServiceURL(
    `https://${STORAGE_ACCOUNT_NAME}.blob.core.windows.net`,
    pipeline
  );
const uploadOptions = {
    bufferSize: FOUR_MEGABYTES,
    maxBuffers: 20,
};
const aborter = Aborter.timeout(30 * ONE_MINUTE);
var stream = require('stream');


async function getReceipt(claims, trxId, dtStr, receipt_name, output) {
    try {
        var scp = claims['scp']
         var username = claims['name']
         if(scp.split(" ").indexOf(process.env.ReadScope) >= 0) {
            var dt = new Date(dtStr)
            var dtMonth = dt.getMonth() + 1
            const containerURL = ContainerURL.fromServiceURL(serviceURL, username);
            const blobPrefix = dt.getFullYear() + '/' + dtMonth + '/' + dt.getDate() + '/' + trxId + '/' + receipt_name

            marker = undefined;
         //   const receipts = []
         //   do {
            const listBlobsResponse = await containerURL.listBlobFlatSegment( 
            aborter,
            marker,
            {
                prefix: blobPrefix
            });

            marker = listBlobsResponse.nextMarker;

            if(listBlobsResponse.segment.blobItems.length == 0) {
                return output({code: 404, msg: 'Receipt Not Found!'})
            }

            var item = listBlobsResponse.segment.blobItems[0]
            const blobURL = BlobURL.fromContainerURL(containerURL, item.name);
            var blobProperties = await blobURL.getProperties(aborter).catch(function(err) {
                output({code: 500, msg: 'Error getting receipt details. Please try again. Error Details: ' + err})
            })
            const buf = Buffer.alloc(blobProperties.contentLength + 10);
            await downloadBlobToBuffer(
                aborter,
                buf,
                blobURL,
                0,
                0,
                {
                    blockSize: 4 * 1024 * 1024,
                    maxRetryRequestsPerBlock: 5,
                    parallelism: 20
                }
            ).catch(function(err) {
                return output({code: 500, msg: 'Error Downloading blob. Please try again. Error Details: ' + err})
            })

            var receipt = buf.toString('base64')
            return output({code: 200, msg: receipt})
         } else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    }  catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.getReceipt = getReceipt;

async function postReceipt(claims, trxId, body, output) {
    try {
        var scp = claims['scp']
        var username = claims['name']
        if(scp.split(" ").indexOf(process.env.WriteScope) >= 0) {
            var dt = new Date(body.trx_date)
            var dtMonth = dt.getMonth() + 1
            var blobPrefix = dt.getFullYear() + '/' + dtMonth + '/' + dt.getDate() + '/' + trxId + '/'
            await body.receipts.map(async receipt => {
                   var blobPath = blobPrefix + receipt.name;
                   
                   var containerUrl = ContainerURL.fromServiceURL(serviceURL, username);
                   var blobURL = BlobURL.fromContainerURL(containerUrl, blobPath);
                   var blockBlobURL = BlockBlobURL.fromBlobURL(blobURL);
            
                   var bufferStream = new stream.PassThrough();
                   bufferStream.end(new Buffer(receipt.body, 'base64'));
                   bufferStream.pipe( process.stdout );
        
                   await uploadStreamToBlockBlob(aborter, 
                    bufferStream, 
                    blockBlobURL, 
                    uploadOptions.bufferSize, 
                    uploadOptions.maxBuffers);
            }).catch(function(err) {
                output({code: 404, msg: 'Failed to post receipt. Container not found. Error details: ' + err})
            });

            var sales_tax = body.sales_tax
            var grand_total = body.grand_total
            var updateStmt = "UPDATE dbo.transactions SET receipt_link = @receipt_link WHERE id = @id; " + 
                                "SELECT id, receipt_link, updated_ytd FROM dbo.transactions WHERE id= @id and sales_tax = @sales_tax and grand_total = @grand_total and username = @username;"
            
            var results = await tds.default.sql(updateStmt)
                    .parameter('receipt_link', TYPES.NVarChar, blobPrefix)
                    .parameter('id', TYPES.Int, parseInt(trxId))
                    .parameter('username', TYPES.NVarChar, username)
                    .parameter('sales_tax',TYPES.Decimal, parseFloat(sales_tax), {scale:2})
                    .parameter('grand_total', TYPES.Decimal, parseFloat(grand_total), {scale:2})
                    .execute(false)
            
            if(results.length == 0) {   
                throw "Incorrect information. No such transaction exits with id: " + trxId + "; sales_tax: " + body.sales_tax + "; grand_total: " + body.grand_total
            }

            var updated_ytd = functions.parseBoolean(results[0].updated_ytd)
            if(!updated_ytd) {
                var updatedYTDTax = await updateYTDTotal_Internal(username, trxId, dt, parseFloat(body.sales_tax), parseFloat(body.grand_total));
                sales_tax = updatedYTDTax[0].sales_tax
                grand_total = updatedYTDTax[0].grand_total
            } else {
                let updatedYTDTax = await aggController.getYTDTotal_Internal(username, dt.getFullYear());
                if(updatedYTDTax.length != 0) {
                    sales_tax = updatedYTDTax[0].SalesTax
                    grand_total = updatedYTDTax[0].GrandTotal
                } 
            }
            
            var retVal = {
                id: trxId,
                ReceiptLink: blobPrefix,
                YTDSalesTax: sales_tax,
                TTDGrandTotal: grand_total
            }
            return output({code: 200, msg: retVal})
         } else {
            return output({code: 500, msg: 'Invalid Scope'})
         }
    } catch (ex) {
            return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.postReceipt = postReceipt


async function deleteReceipt_Internal(username, receipt_prefix, receipt_name) {
    return new Promise(async (resolve, reject) => {
        const containerURL = ContainerURL.fromServiceURL(serviceURL, username);
        var total_receipts = 0
        var deleted_receipts = 0
        if(receipt_name == null) {
            marker = undefined;
            receipts = []
            do {
                const listBlobsResponse = await containerURL.listBlobFlatSegment( 
                    aborter,
                    marker,
                    {
                        prefix: receipt_prefix
                    }
                );

                marker = listBlobsResponse.nextMarker;
                total_receipts += listBlobsResponse.segment.blobItems.length
                listBlobsResponse.segment.blobItems.map(blob => {
                    const blobURL = BlobURL.fromContainerURL(containerURL, blob.name);
                    blobURL.delete(aborter)
                        .then(function(results) {
                            deleted_receipts++ 
                        }, function(err){
                            reject('Error Deleting Receipts. Details: ' + err); 
                        })
                });
                if(total_receipts == 0) {
                    resolve('No receipts to delete')
                }
                if(total_receipts.length == deleted_receipts.length) {
                    resolve('Receipt(s) Successfully Deleted')
                } else {
                    reject('Internal Error: Not all receipts got deleted')
                }
            } while (marker);
        } else {
            const blobURL = BlobURL.fromContainerURL(containerURL, receipt_prefix + receipt_name);
                blobURL.delete(aborter)
                .then(function(result) {
                    resolve('Receipt(s) Successfully Deleted');
                }, function(err) {
                    reject('Error Deleting Receipt. Details: ' + err); 
            })
        }
    });
}

async function deleteReceipt(claims, trxId, body, output) {
    try {
        var scp = claims['scp']
         var username = claims['name']
         if(scp.split(" ").indexOf(process.env.WriteScope) >= 0) {
             if(body.receipt_link != null) {
                var receipt_name = body.receipt_name || null;
                var deleteReceipt = await deleteReceipt_Internal(username, body.receipt_link, receipt_name)
                return output({code: 200, msg: deleteReceipt})
             } else {
                return output({code: 403, msg: 'Invalid receipt_link'})
             }
         } else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    }  catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.deleteReceipt = deleteReceipt;


async function updateYTDTotal_Internal(username, trxId, dt, sales_tax, grand_total, updated_ytd){
    return new Promise(async (resolve, reject) => {
        try {
            new tds.TdsConnection().transaction(async (conn) => {
                var sproc = await conn.sql('dbo.update_ytdtotal')
                    .parameter('username', TYPES.NVarChar, username)
                    .parameter('year', TYPES.Int, dt.getFullYear())
                    .parameter('sales_tax', TYPES.Decimal, parseFloat(sales_tax), {scale:2})
                    .parameter('grand_total', TYPES.Decimal, parseFloat(grand_total), {scale:2})
                    .execute(false, true)

                var updatedYTD = functions.parseBoolean(updated_ytd) || false
                if(sproc.length == 1 && updatedYTD != null && !updatedYTD) { 
                    var updateStmt = "UPDATE dbo.transactions SET updated_ytd = @updated_ytd WHERE id = @id and username = @username; "
                    var test = conn.sql(updateStmt) 
                    .parameter('id', TYPES.Int, parseInt(trxId))
                    .parameter('username', TYPES.NVarChar, username)
                    .parameter('updated_ytd', TYPES.Bit, true)
                    
                    await test.execute(false, false).catch(function(err){
                        reject(err.toString())
                    })
                    
                }

                resolve(sproc.map(row => {
                    return {
                        sales_tax: row.sales_tax,
                        grand_total: row.grand_total
                    }
                }));
            },
            () => {},
            (ex) => {
                reject(ex) 
            });
        } catch (ex) {
            reject(ex.stack)
        }
    });
}
module.exports.updateYTDTotal_Internal = updateYTDTotal_Internal;