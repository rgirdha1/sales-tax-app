var TYPES = require('tedious').TYPES;
var ConnectionPool = require('tedious-connection-pool');
var Tedious = require('tedious');
var Request = Tedious.Request;
var Connection = Tedious.Connection;
const receiptController = require('./receiptController')
const functions = require('../utils/functions')
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').load();
}
const tds = require('../utils/tds-promises');

async function getLastTrx (claims, trxId, output) {
    try {
        var scp = claims['scp']
        var username = claims['name']
        if(scp.split(" ").indexOf(process.env.ReadScope) >= 0) {
            var sqlStmt = ""
            var trx_id = 0
            if(trxId != null) {
                sqlStmt = "SELECT * from dbo.transactions a where a.id = @trxId and a.username = @username";
                trx_id = parseInt(trxId)
            } else {
                sqlStmt = "SELECT * from dbo.transactions a where a.id = (SELECT IDENT_CURRENT('transactions')) and a.username = @username";
            }
            let results = await tds.default.sql(sqlStmt)
                .parameter('username', TYPES.NVarChar, username)
                .parameter('trxId', TYPES.Int, trx_id)
                .executeImmediate();
            if (!results || results.length != 1) {
                return output({code: 404, msg: "Unable to find any transactions for user: " + username});
            } else {
                return output({code: 200, msg: results.map(row => {
                        return {
                            Id: row.id,
                            TransactionDate: row.trx_date,
                            Category: row.category,
                            Memo: row.memo,
                            SalesTax: row.sales_tax,
                            GrandTotal: row.grand_total,
                            Receipt: row.receipt_link,
                            updatedYTD: row.updatedYTD
                        };
                    })
                });
            }
        }  else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    } catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.getLastTrx = getLastTrx;

async function postNewTrx (claims, body, output) {
    try {
       var scp = claims['scp']
        var username = claims['name']
        if(scp.split(" ").indexOf(process.env.WriteScope) >= 0) {
            new tds.TdsConnection().transaction(async (connection) => {
                var sqlStmt = "INSERT INTO dbo.transactions(username, entry_datetime, trx_date, category, memo, sales_tax, grand_total, updated_ytd) " + 
                        "VALUES (@username, @entry_datetime, @trx_date, @category, @memo, @sales_tax, @grand_total, @updated_ytd);" +
                        "SELECT id, username, trx_date, category, memo, sales_tax, grand_total FROM dbo.transactions WHERE id= SCOPE_IDENTITY() and username= @username;";

                if(body.id != null) {
                    sqlStmt = "UPDATE dbo.transactions SET trx_date = @trx_date, category = @category, memo = @memo, sales_tax = @sales_tax, grand_total = @grand_total, updated_ytd = @updated_ytd " +
                        "WHERE id = @id; " + 
                        "SELECT id, username, trx_date, category, memo, sales_tax, grand_total FROM dbo.transactions WHERE id= @id and username= @username;";
                }

                var dt = new Date(body.trx_date)

                var insertQry = connection.sql(sqlStmt)
                    .parameter('username', TYPES.NVarChar, username)
                    .parameter('trx_date', TYPES.Date, dt)
                    .parameter('category', TYPES.NVarChar, body.category)
                    .parameter('memo', TYPES.NVarChar, body.memo)
                    .parameter('sales_tax', TYPES.Decimal, parseFloat(body.sales_tax), {scale:2})
                    .parameter('grand_total', TYPES.Decimal, parseFloat(body.grand_total), {scale:2})
                
                if(body.entry_datetime != null) {
                    insertQry.parameter('entry_datetime', TYPES.DateTimeOffset, new Date(body.entry_datetime))
                }
                if(body.id != null) {
                    insertQry.parameter('id', TYPES.Int, body.id)
                }
                if(body.updated_ytd != null) {
                    insertQry.parameter('updated_ytd', TYPES.Bit, functions.parseBoolean(body.updated_ytd) )
                } else {
                    insertQry.parameter('updated_ytd', TYPES.Bit, false)
                }
                
                var results = await insertQry.execute(false)

                var id = results[0].id
                var receipt_link = null

                var sales_tax_diff = parseFloat(body.sales_tax) - parseFloat(body.prev_sales_tax) || 0
                var grand_total_diff = parseFloat(body.grand_total) - parseFloat(body.prev_grand_total) || 0
                var updatedYTD = functions.parseBoolean(body.updated_ytd) || false

                var res = results.map(trx => {
                    return {
                        id: trx.id,
                        username: username,
                        trx_date: trx.trx_date,
                        category: trx.category,
                        memo: trx.memo,
                        sales_tax: trx.sales_tax,
                        grand_total: trx.grand_total,
                        receipt_link: receipt_link
                    };
                });

                if(updatedYTD && (sales_tax_diff != 0 || grand_total_diff != 0)) {
                    try{
                        var updatedTotal = await receiptController
                                                    .updateYTDTotal_Internal(username, id.toString(), dt, sales_tax_diff, grand_total_diff, updatedYTD)
                                                    .catch(function(err) {
                                                        return output({code: 500, msg: err})
                                                    })
                        return output({code: 200, msg: res[0]});
                    } catch (ex) {
                        throw 'Error updating YTD Total. Error Details: ' + ex                    
                    }
                } else {
                    return output({code: 200, msg: res[0]});
                }
            },
            (results) => ({}),
            (ex) => output({code:500, msg: "Failed to add transaction: " + ex}));
        } else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    }  catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.postNewTrx = postNewTrx;

async function deleteTrx(claims, trxId, trx_date, output){
    try {
        var scp = claims['scp']
        var username = claims['name']
        if(scp.split(" ").indexOf(process.env.WriteScope) >= 0) {
            if(trxId != null) {
                new tds.TdsConnection().transaction(async (connection) => {
                    var selectStmt = "SELECT * from dbo.transactions WHERE id = @trxId and username = @username";
                    let results = await connection.sql(selectStmt)
                        .parameter('trxId', TYPES.Int, parseInt(trxId))
                        .parameter('username', TYPES.NVarChar, username)
                        .execute(false);
                    if(results.length > 0) {
                        var sqlStmt = "DELETE FROM dbo.transactions WHERE id = @trxId and username = @username";
                        await tds.default.sql(sqlStmt)
                            .parameter('trxId', TYPES.Int, parseInt(trxId))
                            .parameter('username', TYPES.NVarChar, username)
                            .execute(false).catch(function(err){
                                output({code: 500, msg:'Error deleting transaction. Details: ' + err})
                            });
                        var updatedYTD = functions.parseBoolean(results[0].updated_ytd) || false
                        if(updatedYTD) {
                            try {
                                await receiptController.updateYTDTotal_Internal(
                                    username, 
                                    trxId, 
                                    trx_date, 
                                    results[0].sales_tax, 
                                    results[0].grand_total)
                                if(results[0].receipt_link != null){
                                    await receiptController.deleteReceipt_Internal(
                                        username, 
                                        results[0].receipt_link, 
                                        null)
                                }     
                            } catch (ex) {
                                return output({code: 500, msg: 'Error deleting receipt or updating total. Error Details: ' + ex})
                            }
                            return output({code: 200, msg: "Transaction Deleted along with the receipt"});
                        } else {
                            return output({code: 200, msg: 'Transaction deleted!'})
                        }
                    } else {
                        return output({code: 404, msg: 'Transaction not found!'})
                    }      
                },
                (results) => ({}),
                (ex) => output({code:500, msg: "Failed to add transaction: " + ex}));
            } else {
                return output({code: 500, msg: 'Transaction ID cannot be null'})
            }
        }
        else {
            return output({code: 500, msg: 'Invalid Scope'})
        }
    }  catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.deleteTrx = deleteTrx;