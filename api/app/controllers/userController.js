var TYPES = require('tedious').TYPES;
var ConnectionPool = require('tedious-connection-pool');
var Tedious = require('tedious');
var Request = Tedious.Request;
var Connection = Tedious.Connection;
const aggController = require('./aggregationController')
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').load();
}
const tds = require('../utils/tds-promises');
const {
    SharedKeyCredential,
    uploadStreamToBlockBlob,
    downloadBlobToBuffer,
    Aborter,
    BlobURL,
    BlockBlobURL,
    ContainerURL,
    ServiceURL,
    StorageURL
  } = require("@azure/storage-blob");

const STORAGE_ACCOUNT_NAME = process.env.storageAccName;
const ACCOUNT_ACCESS_KEY = process.env.storageAccKey;
const ONE_MEGABYTE = 1024 * 1024;
const FOUR_MEGABYTES = 4 * ONE_MEGABYTE;
const ONE_MINUTE = 60 * 1000;
const credentials = new SharedKeyCredential(STORAGE_ACCOUNT_NAME, ACCOUNT_ACCESS_KEY);
const pipeline = StorageURL.newPipeline(credentials, {
    retryOptions: {maxTries: 4}
});
const serviceURL = new ServiceURL(
    `https://${STORAGE_ACCOUNT_NAME}.blob.core.windows.net`,
    pipeline
  );
const uploadOptions = {
    bufferSize: FOUR_MEGABYTES,
    maxBuffers: 20,
};
const aborter = Aborter.timeout(30 * ONE_MINUTE);


async function newUserInit(claims, output){
    try {
        var scp = claims['scp']
         var username = claims['name']
         if(scp.split(" ").indexOf(process.env.WriteScope) >= 0) {
            const containerURL = ContainerURL.fromServiceURL(serviceURL, username);
            await containerURL.create(aborter).catch(function(err) {
                return output({code: 500, msg: 'Error allocating storage. Details: ' + err});
            });

            return output({code: 200, msg: 'Successfully initialized and allocated storage space for user: ' + username});
         } else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    }  catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.newUserInit = newUserInit;


async function deleteAll(claims, output) {
    try {
        var scp = claims['scp']
         var username = claims['name']
         if(scp.split(" ").indexOf(process.env.WriteScope) >= 0) {
            new tds.TdsConnection().transaction(async (connection) => {
                var deleteTrx = "DELETE FROM dbo.transactions where username = @username; " +
                "SELECT * from dbo.transactions where username = @username"
                var deleteResults = await connection.sql(deleteTrx)
                    .parameter('username', TYPES.NVarChar, username)
                    .execute(false)
                
                if(deleteResults.length != 0) {
                    output({code: 500, msg: 'Error deleting transactions! Please try again.'})
                }

                var deleteYtdTotal = "DELETE from dbo.ytdtotal where username = @username; " +
                "SELECT * from dbo.ytdtotal where username = @username"
                var ytdResults = await connection.sql(deleteYtdTotal)
                    .parameter('username', TYPES.NVarChar, username)
                    .execute(false)

                if(ytdResults.length != 0) {
                    return output({code: 500, msg: 'Error deleting YTD Total! Please try again.'})
                }

                const containerURL = ContainerURL.fromServiceURL(serviceURL, username);
                await containerURL.delete(aborter).catch(function(err) {
                    return output({code: 500, msg: 'Error deallocating storage. Please try again. Details: ' + err});
                })

                return output({code: 200, msg: 'All User data deleted. Good bye.'})
            }, (results) => ({}),
            (ex) => output({code:500, msg: "Failed to delete user transactions. Details: " + ex}));
         } else {
            return output({code: 500, msg: 'Invalid Scope'})
        } 
    }  catch (ex) {
        return output({code: 500, msg:'Internal Error: ' + ex});
    }
}
module.exports.deleteAll = deleteAll;