IF OBJECT_ID('dbo.transactions', 'U') IS NOT NULL 
  DROP TABLE dbo.transactions;

IF OBJECT_ID('dbo.categories', 'U') IS NOT NULL 
  DROP TABLE dbo.categories;

IF OBJECT_ID('dbo.ytdtotal', 'U') IS NOT NULL 
  DROP TABLE dbo.ytdtotal;

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[update_ytdtotal]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dbo.update_ytdtotal

CREATE TABLE [dbo].[categories](
	[category] [nvarchar](50) NOT NULL,
	[description] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

INSERT INTO dbo.[categories] VALUES ('Groceries', 'Amount you spend on groceries at supermarkets')
INSERT INTO dbo.[categories] VALUES ('Restaurants & Bars', 'Amount you spend on eating out and going out')
INSERT INTO dbo.[categories] VALUES ('Home Shopping', 'Amount you spend on furniture or other items for your home')
INSERT INTO dbo.[categories] VALUES ('Clothes Shopping', 'Amount you spend on clothing and other personal items')
INSERT INTO dbo.[categories] VALUES ('Electronics', 'Amount you spend on electronics such as headphones or a news TV')
INSERT INTO dbo.[categories] VALUES ('Other Shopping', 'Amount you spend on groceries at supermarkets')
INSERT INTO dbo.[categories] VALUES ('Big Ticket Items', 'Big ticket items such as cars, boats, a house, etc.')
INSERT INTO dbo.[categories] VALUES ('Misc.', 'Amount you spend on misc. items such as gym membership fees, etc.')


CREATE TABLE [dbo].[transactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[entry_datetime] [datetimeoffset](7) NOT NULL,
	[trx_date] [date] NULL,
	[category] [nvarchar](50) NULL,
	[memo] [nvarchar](max) NULL,
	[sales_tax] [decimal](5, 2) NOT NULL,
	[grand_total] [decimal](5, 2) NULL,
	[receipt_link] [nvarchar](max) NULL,
	[updated_ytd] [bit] NULL,
 CONSTRAINT [C_PK_trx_id] PRIMARY KEY CLUSTERED 
(
	[id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[transactions]  WITH CHECK ADD FOREIGN KEY([category])
REFERENCES [dbo].[categories] ([category])
GO

CREATE INDEX idx_trx_id
ON dbo.transactions (id)


CREATE TABLE [dbo].[ytdtotal](
	[username] [nvarchar](50) NOT NULL,
	[year] [int] NOT NULL,
	[sales_tax] [decimal](5, 2) NOT NULL,
	[grand_total] [decimal](5, 2) NOT NULL,
 CONSTRAINT [C_PK_ytdtotal_id] PRIMARY KEY CLUSTERED 
(
	[username] ASC,
	[year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

CREATE PROCEDURE [dbo].[update_ytdtotal] @username nvarchar(50), @year int,  @sales_tax decimal(5,2), @grand_total decimal(5,2)
AS
BEGIN
	DECLARE @current_sales_tax decimal(5,2),
			@current_grand_total decimal(5,2)

	SELECT @current_sales_tax = 0
	SELECT @current_grand_total = 0

	SELECT @current_sales_tax = sales_tax, @current_grand_total = grand_total FROM ytdtotal
	WHERE username = @username and [year] = @year

	DECLARE @new_sales_tax decimal(5,2)
	DECLARE @new_grand_total decimal(5,2)

	SELECT @new_sales_tax = @current_sales_tax + @sales_tax
	SELECT @new_grand_total = @current_grand_total + @grand_total

	UPDATE dbo.ytdtotal
		SET sales_tax = @new_sales_tax, grand_total = @new_grand_total
		WHERE username = @username and [year] = @year
	
	IF @@ROWCOUNT=0
		INSERT INTO dbo.ytdtotal(username, [year], sales_tax, grand_total) VALUES (@username, @year, @new_sales_tax, @new_grand_total)

	SELECT @new_sales_tax as 'sales_tax', @new_grand_total as 'grand_total'
END
GO